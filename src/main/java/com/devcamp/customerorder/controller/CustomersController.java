package com.devcamp.customerorder.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.aspectj.apache.bcel.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customerorder.model.CCustomer;
import com.devcamp.customerorder.model.COrder;
import com.devcamp.customerorder.responsitory.ICustomerResponsitory;


@CrossOrigin
@RestController

public class CustomersController {
    @Autowired
    ICustomerResponsitory pCustomerResponsitory;


    //Viết API lấy danh sách customers/ devcamp-customers

    @GetMapping("/devcamp-customers")
    public ResponseEntity<List<CCustomer>> getCustomerList(){
        try {
            //  khởi tạo 1 đối tượng array list customer
            List<CCustomer> customerList = new ArrayList<CCustomer>();
            //thêm tất cả arr lấy từ sql vào customerList  
            pCustomerResponsitory.findAll().forEach(customerList::add);
            return new ResponseEntity<>(customerList, HttpStatus.OK);
            
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR); // báo lỗi máy chủ nội bộ 
        }
    }

    //Viết API lấy danh sách orders truyền vào customerId 

    @GetMapping("/devcamp-orders")
    public ResponseEntity<Set<COrder>> getOrderListCustomerId(@RequestParam(value = "customerId") long customerId){
        // set giống list nhưng các giá trị của set là duy nhất không có sự trụng lặp 
        try {
            // dựa vào id để tìm ra customer . trả về đối tượng   vCustomer
            CCustomer vCustomer = pCustomerResponsitory.findById(customerId);
            // nếu  vCustomer khác null thì trả về array list các order 
            if(vCustomer != null){
                return new ResponseEntity<> (vCustomer.getOrders(), HttpStatus.OK);
            }
            else  {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND); //Nếu không tìm thấy thông tin về loại xe cho mã xe được truyền vào, phương thức sẽ trả về mã trạng thái HTTP 
                //là NOT_FOUND và giá trị của đối tượng ResponseEntity là null.
            }
            
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR); // báo lỗi máy chủ nội bộ 
        }
    }

    
}
