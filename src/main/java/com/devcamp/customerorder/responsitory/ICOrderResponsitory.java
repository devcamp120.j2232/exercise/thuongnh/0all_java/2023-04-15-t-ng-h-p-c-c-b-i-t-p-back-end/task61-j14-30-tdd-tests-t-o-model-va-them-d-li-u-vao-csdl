package com.devcamp.customerorder.responsitory;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.customerorder.model.COrder;

public interface ICOrderResponsitory extends  JpaRepository<COrder, Long>  {
    
}
