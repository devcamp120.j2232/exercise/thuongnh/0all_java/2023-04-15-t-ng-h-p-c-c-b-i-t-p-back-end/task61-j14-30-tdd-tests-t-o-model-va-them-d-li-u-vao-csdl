package com.devcamp.customerorder.responsitory;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.customerorder.model.CCustomer;
import com.devcamp.customerorder.model.COrder;

public interface ICustomerResponsitory extends JpaRepository<CCustomer, Long> {
    // class này là công cụ để giao tiếp với sql
    CCustomer  findById(long id);
    
}
